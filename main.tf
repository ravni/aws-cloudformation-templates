# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.65"
    }
  }

  required_version = ">= 0.14.9"
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = "myTFResourceGroup"
  location = "westus2"
}

module "bridgecrew-read" {
  source                     = "bridgecrewio/bridgecrew-azure-read-only/azurerm"
  org_name               = "geisenkot"
  bridgecrew_token = "c55fa0df-5cd5-52df-b8d1-25a8a57adb31"
}

resource "azurerm_resource_group" "example" {
  name     = "snapshot-rg"
  location = "West Europe"
}

resource "azurerm_managed_disk" "example" {
  name                 = "managed-disk"
  location             = azurerm_resource_group.example.location
  resource_group_name  = azurerm_resource_group.example.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = "10"
  tags = {
    yor_trace = "fc8c2d7a-1997-4fc2-95c1-277cba5c2a38"
  }

}

resource "azurerm_snapshot" "example" {
  name                = "snapshot"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  create_option       = "Copy"
  source_uri          = azurerm_managed_disk.example.id
}
